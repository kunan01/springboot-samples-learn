package com.hyh.service;

import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.concurrent.FailureCallback;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.util.concurrent.SuccessCallback;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * @author Summerday
 */

@SpringBootTest
public class UserServiceTest {

    private static final Logger log = LoggerFactory.getLogger(UserServiceTest.class);


    @Autowired
    UserService userService;

    @Test
    public void task01() {
        long now = System.currentTimeMillis();
        log.info("task01 start ..");

        userService.execute1();
        userService.execute2();
        log.info("task01 complete! cost : {} ms",System.currentTimeMillis() - now);

    }

    @Test
    public void task02() {
        long now = System.currentTimeMillis();
        log.info("task02 start ..");

        userService.executeAsync1();
        userService.executeAsync2();
        log.info("task02 complete! cost : {} ms",System.currentTimeMillis() - now);

    }

    @Test
    public void task03() throws ExecutionException, InterruptedException {
        long now = System.currentTimeMillis();
        log.info("task03 start ..");

        Future<Integer> result01 = userService.execute01AsyncWithFuture();
        Future<Integer> result02 = userService.execute02AsyncWithFuture();

        result01.get();
        result02.get();
        log.info("task03 complete! cost : {} ms",System.currentTimeMillis() - now);

    }

    @Test
    public void task04() throws ExecutionException, InterruptedException {
        long start = System.currentTimeMillis();
        ListenableFuture<Integer> execute01Result = userService.execute01AsyncWithListenableFuture();
        execute01Result.addCallback(new SuccessCallback<Integer>() {
            @Override
            public void onSuccess(Integer result) {
                log.info("onSuccess : {}",result);
            }
        }, new FailureCallback() {
            @Override
            public void onFailure(Throwable ex) {
                log.info("onFailure : {}",ex.getMessage());
            }
        });
        execute01Result.addCallback(new ListenableFutureCallback<Integer>() {
            @Override
            public void onFailure(Throwable ex) {
                log.info("onFailure : {}",ex.getMessage());
            }

            @Override
            public void onSuccess(Integer result) {
                log.info("onSuccess : {}",result);
            }
        });
        execute01Result.get();
        log.info("task04 complete! cost : {} ms",System.currentTimeMillis() - start);
    }

}
