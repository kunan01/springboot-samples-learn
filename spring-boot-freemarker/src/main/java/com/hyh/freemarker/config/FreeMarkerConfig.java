package com.hyh.freemarker.config;

import com.hyh.freemarker.template.StringTemplate;
import com.hyh.freemarker.template.TimeAgoMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * @author Summerday
 */
@Configuration
public class FreeMarkerConfig {

    @Autowired
    StringTemplate stringTemplate;
    @Autowired
    private freemarker.template.Configuration configuration;


    @PostConstruct
    public void setUp() {
        configuration.setSharedVariable("timeAgo", new TimeAgoMethod());
        configuration.setSharedVariable("strstr", stringTemplate);
    }
}
