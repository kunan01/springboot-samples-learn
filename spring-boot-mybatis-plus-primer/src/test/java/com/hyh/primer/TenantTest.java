package com.hyh.primer;

import com.hyh.primer.entity.User;
import com.hyh.primer.mapper.UserMapper;
import com.hyh.primer.service.UserService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
class TenantTest {

    @Resource
    UserMapper userMapper;

    @Test
    void select(){
        // manager_id = 1088248166370832385
        List<User> users = userMapper.selectList(null);
        users.forEach(System.out::println);
    }
}
