package com.hyh.mybatisplus.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author Summerday
 */
@Data
@ToString
@TableName("user") //指定表名
@EqualsAndHashCode(callSuper = false)
public class User extends Model<User> {

    @TableId(type = IdType.AUTO) //指定主键
    private Long id;

    @TableField(value = "name",condition = SqlCondition.LIKE) //指定字段名
    private String name;
    private String email;
    private Integer age;

    private Long managerId;

    @TableField("create_time")
    private Date createTime;

    @TableField(exist = false)//备注[数据库中无对应的字段]
    private String remark;
}
