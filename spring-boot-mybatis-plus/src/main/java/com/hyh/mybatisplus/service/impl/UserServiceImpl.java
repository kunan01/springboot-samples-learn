package com.hyh.mybatisplus.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hyh.mybatisplus.entity.User;
import com.hyh.mybatisplus.mapper.UserMapper;
import com.hyh.mybatisplus.service.UserService;
import org.springframework.stereotype.Service;

/**
 * Service实现类,继承ServiceImpl,实现接口
 * @author Summerday
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}
