package com.hyh.mybatisplus;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.hyh.mybatisplus.entity.User;
import com.hyh.mybatisplus.mapper.UserMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Summerday
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class DeleteTest {

    @Resource
    private UserMapper userMapper;

    @Test
    public void deleteById(){
        int row = userMapper.deleteById(1319853504134770689L);
        System.out.println("影响记录数 : " + row);
    }

    @Test
    public void deleteByMap(){
        Map<String,Object> map = new HashMap<>();
        map.put("name","天");
        int row = userMapper.deleteByMap(map);
        System.out.println("影响记录数 : " + row);
    }

    /**
     * 批量删除
     */
    @Test
    public void deleteByIds(){
        int row = userMapper.deleteBatchIds(Arrays.asList(1, 2));
        System.out.println("影响记录数 : " + row);
    }

}
