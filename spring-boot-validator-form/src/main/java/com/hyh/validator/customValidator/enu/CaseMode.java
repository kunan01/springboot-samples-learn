package com.hyh.validator.customValidator.enu;

/**
 * 定义枚举类CaseMode
 * @author Summerday
 */
public enum CaseMode {
    UPPER,
    LOWER;
}