package com.hyh.exception;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartException;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Summerday
 */
@ControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(MultipartException.class)
    public String handle(MultipartException e, RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("msg", e.getMessage());
        return "redirect:uploadStatus";
    }


    @ResponseBody
    @ExceptionHandler(Exception.class)
    public Map<String,Object> handle(Exception e) {
        Map<String, Object> map = new HashMap<>(2);
        map.put("msg",e.getMessage());
        return map;
    }
}
